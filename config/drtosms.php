<?php

return [



    'middleware' => [
        'receive_action_middleware' => [],
        'template_action_middleware' => []
    ],

    'driver' => env('SMS_DRIVER', 'kavenegar'),

    'template_tokens' => [':NAME',':DATE',':TIME',':NUMBER'],

    'user_mobile' => 'mobile',


    // class name that implemented OnReceive interface
    //TODO: we should consider for every driver separately
    'on_receive' => '',

    'drivers' =>[
        'kavenegar' => [
            'api_key' => env('KAVENEGAR_SMS_API_KEY', ''),
            'sender' => env('KAVENEGAR_SENDER_NUMBER','')
        ],
        'payam' => [
            'api_key' => env('PAYAM_SMS_API_KEY', ''),
            'sender' => env('PAYAM_SENDER_NUMBER','')
        ]
    ]
];