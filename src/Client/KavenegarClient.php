<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 4:14 PM
 */

namespace Doctoreto\SMS\Client;

use GuzzleHttp\Client;
use Doctoreto\SMS\SMSClient;
use Illuminate\Http\Request;

class KavenegarClient extends SMSClient
{

    private $url = 'https://api.kavenegar.com/v1/:api-key/sms';

    private $apiKey;

    public function __construct(Client $client)
    {
        parent::__construct($client);
        $this->apiKey = config('drtosms.drivers.kavenegar.api_key');

        $this->url = str_replace(':api-key', $this->apiKey, $this->url);
    }

    public function receive(Request $request): Request
    {
        return $request;
    }


    public function send(string $receptor, string $sender, string $message)
    {
        return $this->sendSingleRequest($receptor, $sender, $message);
    }

    public function sendArray(array $receptors, string $sender, array $messages)
    {
        $senders = [];
        foreach ($receptors as $key => $receptor){
            $senders[$key] = $sender;
        }

        return $this->sendManyRequest($receptors, $senders, $messages);
    }

    public function sendToMany(array $receptors, string $sender, string $message)
    {
        $senders = array_fill(0, count($receptors), $sender);
        $messages = array_fill(0, count($receptors), $message);

        return $this->sendManyRequest($receptors, $senders, $messages);
    }

    private function sendManyRequest(array $receptors, array $senders, array $messages)
    {
        $senders = $this->kevenegarArrayParam($senders);
        $messages = $this->kevenegarArrayParam($messages);
        $receptors = $this->kevenegarArrayParam($receptors);

        $url = $this->url . '/sendarray.json';

        return $this->request($url, $receptors, $senders, $messages);
    }

    private function sendSingleRequest(string $receptor, string $sender, string $message)
    {
        $url = $this->url . '/send.json';

        return $this->request($url, $receptor, $sender, $message);
    }

    private function request(string $url, string $receptor, string $sender, string $message)
    {
        if (is_null($this->apiKey) || empty($this->apiKey)) {
            throw new \InvalidArgumentException('In order to send sms via kavenegar you need to add credentials in the `kavenegar` key of `config.SMS_API_KEY`.');
        }

        $res = $this->client->request('POST', $url, [
            'form_params' => [
                'receptor' => $receptor,
                'sender' => $sender,
                'message' => $message
            ],
            'headers' => [
                'Accept' => 'application/json',
                'content-type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        return \GuzzleHttp\json_decode($res->getBody()->getContents(),true);
    }

    private function kevenegarArrayParam(array $values)
    {

        foreach ($values as $key => $value) {
            $values[$key] = sprintf('"%s"',$value);
        }

        return sprintf('[%s]', implode(',', $values));

    }

    public function getMultipleStatus(array $messageIds)
    {
        if (is_null($this->apiKey) || empty($this->apiKey)) {
            throw new \InvalidArgumentException('In order to send sms via kavenegar you need to add credentials in the `kavenegar` key of `config.SMS_API_KEY`.');
        }
        $url = $this->url . '/status.json';
        $res = $this->client->request('POST', $url, [
            'form_params' => [
                'messageid' => implode(',',$messageIds)
            ],
            'headers' => [
                'Accept' => 'application/json',
                'content-type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        return \GuzzleHttp\json_decode($res->getBody()->getContents(), true);
    }

    public function getStatus(string $messageId)
    {
        // TODO: Implement getStatus() method.
    }


}