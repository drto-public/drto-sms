<?php
/**
 * Created by PhpStorm.
 * User: MHD
 * Date: 3/1/2020
 * Time: 2:55 AM
 */

namespace Doctoreto\SMS\Client;


use GuzzleHttp\Client;
use Doctoreto\SMS\SMSClient;
use Illuminate\Http\Request;

class PayamClient extends SMSClient
{

    private $url = 'http://payamsms.com/rest';

    private $apiKey;

    public function __construct(Client $client)
    {
        parent::__construct($client);
        $this->apiKey = config('drtosms.drivers.payam.api_key');

    }

    public function receive(Request $request): Request
    {
        return $request;
    }

    public function send(string $receptor, string $sender, string $message)
    {
        $url = $this->url . '/send';

        if (is_null($this->apiKey) || empty($this->apiKey)) {
            throw new \InvalidArgumentException('In order to send sms via payam you need to add credentials in the `payam` driver PAYAM_SMS_API_KEY.');
        }

        $res = $this->client->request('POST', $url, [
            'form_params' => [
                'apikey' => $this->apiKey,
                'to' => $receptor,
                'from' => $sender,
                'content' => $message
            ],
            'headers' => [
                'Accept' => 'application/json',
                'content-type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        return \GuzzleHttp\json_decode($res->getBody()->getContents(), true);
    }

    public function sendArray(array $receptors, string $sender, array $messages)
    {
        $url = $this->url . '/sendMultiple';

        if (is_null($this->apiKey) || empty($this->apiKey)) {
            throw new \InvalidArgumentException('In order to send sms via payam you need to add credentials in the `payam` driver PAYAM_SMS_API_KEY.');
        }

        $params = [
            'apikey' => $this->apiKey,
            'from' => $sender,
        ];

        foreach ($messages as $key => $message) {
            $params['content[' . $key . ']'] = $message;
            $params['to[' . $key . ']'] = $receptors[$key];
        }

        $res = $this->client->request('POST', $url, [
            'form_params' => $params,
            'headers' => [
                'Accept' => 'application/json',
                'content-type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        return \GuzzleHttp\json_decode($res->getBody()->getContents(), true);
    }

    public function sendToMany(array $receptors, string $sender, string $message)
    {
        if (is_null($this->apiKey) || empty($this->apiKey)) {
            throw new \InvalidArgumentException('In order to send sms via payam you need to add credentials in the `payam` driver PAYAM_SMS_API_KEY.');
        }

        $url = $this->url . '/send';

        $res = $this->client->request('POST', $url, [
            'form_params' => [
                'apikey' => $this->apiKey,
                'to' => implode(',', $receptors),
                'from' => $sender,
                'content' => $message
            ],
            'headers' => [
                'Accept' => 'application/json',
                'content-type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        return \GuzzleHttp\json_decode($res->getBody()->getContents(), true);
    }

    public function getStatus(string $messageId)
    {
        if (is_null($this->apiKey) || empty($this->apiKey)) {
            throw new \InvalidArgumentException('In order to send sms via payam you need to add credentials in the `payam` driver PAYAM_SMS_API_KEY.');
        }

        $url = $this->url . '/status?' . 'apikey=' . $this->apiKey . '&messageId=' . $messageId ;

        $res = $this->client->request('GET', $url);

        return \GuzzleHttp\json_decode($res->getBody()->getContents(),true);
    }

    public function getMultipleStatus(array $messageIds)
    {
        // TODO: Implement getMultipleStatus() method.
    }


}