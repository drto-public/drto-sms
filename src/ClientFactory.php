<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 7:39 PM
 */

namespace Doctoreto\SMS;

use Doctoreto\SMS\Client\PayamClient;
use GuzzleHttp\Client;
use Doctoreto\SMS\Client\KavenegarClient;

class ClientFactory
{
    /**
     * @param string $clientName
     * @return SMSClient|null
     */
    public static function getClient(string $clientName)
    {
        switch ($clientName) {
            case 'kavenegar':
                return new KavenegarClient(new Client());
            case 'payam' :
                return new PayamClient(new Client());

            default:
                return null;
        }
    }
}