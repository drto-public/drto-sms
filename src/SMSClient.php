<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 4:35 PM
 */

namespace Doctoreto\SMS;

use GuzzleHttp\Client;
use Doctoreto\SMS\Contract\Client as ClientContract;

abstract class SMSClient implements ClientContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * SMSClient constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }


}