<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 8:59 PM
 */

namespace Doctoreto\SMS\Log;

use Doctoreto\SMS\Contract\Logger as LoggerContract;
use Doctoreto\SMS\Model\SMSLog;

class Logger implements LoggerContract
{
    /**
     * @param array $receptors
     * @param string $sender
     * @param string $messages
     * @param string $type
     * @param string $status
     * @param null $smsableId
     * @param null $smsableClassName
     * @return Illuminate\Support\Collection
     */
    public function log(array $receptors, string $sender, string $messages, string $type, string $status, $smsableId = null, $smsableClassName = null)
    {
        $smsLogs = [];

        foreach ($receptors as $receptor) {
            $smsLogs[] = SMSLog::create([
                'type' => $type,
                'status' => $status,
                'sender' => $sender,
                'receptor' => $receptor,
                'message' => $messages,
                'smsable_id' => $smsableId,
                'smsable_type' => $smsableClassName
            ]);
        }

        return collect($smsLogs);
    }

    public function updateLog(array $logId, string $status)
    {
        SMSLog::whereIn('id', $logId)->update(['status' => $status]);
    }

    public function updateProviderInfoLog(int $logId, string $providerMessageId, string $providerStatus, float $price)
    {
        SMSLog::where('id', '=', $logId)->update([
            'provider_status' => $providerStatus,
            'provider_message_id' => $providerMessageId,
            'price' => $price
        ]);
    }

    public function updateLogBySmsableId(string $smsableId, string $status)
    {
        SMSLog::where('smsable_id', '=', $smsableId)->update(['status' => $status]);
    }

}