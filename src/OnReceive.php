<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 2/3/19
 * Time: 2:49 PM
 */

namespace Doctoreto\SMS;


interface OnReceive
{
    public function onReceive(string $from,string $to,string $content);
}