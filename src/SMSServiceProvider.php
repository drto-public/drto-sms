<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 3:52 PM
 */

namespace Doctoreto\SMS;

use Illuminate\Support\ServiceProvider;

class SMSServiceProvider extends ServiceProvider
{


    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/drtosms.php' => config_path('drtosms.php')
        ], 'config');

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->loadRoutesFrom(__DIR__ . '/../route/routes.php');


        $this->app->bind(SMSClient::class, function () {
            return ClientFactory::getClient(config('drtosms.driver'));
        });

        $onReceiveClass = config('drtosms.on_receive');

        if ($onReceiveClass) {
            $instance = new $onReceiveClass;
            if ($instance instanceof OnReceive) {
                $this->app->bind(OnReceive::class,$onReceiveClass);
            }

        }


    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/drtosms.php', 'drtosms');
    }

}