<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/31/19
 * Time: 1:26 PM
 */

namespace Doctoreto\SMS\SMSTrait;


use Doctoreto\SMS\Model\ReceivedMessage;

trait HasReceivedMessage
{
    public function receivedMessages()
    {
        return $this->hasMany(ReceivedMessage::class, 'to', config('drtosms.user_mobile'));
    }
}