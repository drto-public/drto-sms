<?php

namespace Doctoreto\SMS\Model;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'sms_templates';

    protected $guarded = [];
}
