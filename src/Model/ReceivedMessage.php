<?php

namespace Doctoreto\SMS\Model;

use Illuminate\Database\Eloquent\Model;

class ReceivedMessage extends Model
{
    protected $guarded = [];
}
