<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 4:16 PM
 */

namespace Doctoreto\SMS\Contract;

use Illuminate\Http\Request;

interface Client
{
    /**
     * returns received parameters and transform them to drtosms parameters[from:number,to:number,message:text,messageid]
     *
     * @param Request $request
     * @return array
     */
    public function receive(Request $request): Request;

    public function send(string $receptor, string $sender, string $message);

    public function sendArray(array $receptors, string $sender, array $messages);

    public function sendToMany(array $receptors, string $sender, string $message);

    public function getStatus(string $messageId);

    public function getMultipleStatus(array $messageIds);
}