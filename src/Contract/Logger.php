<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 9:00 PM
 */

namespace Doctoreto\SMS\Contract;


interface Logger
{
    public function log(array $receptors, string $senders, string $messages, string $type, string $status, $smsableId = null, $smsableClassName = null);

    public function updateLog(array $logId, string $status);

    public function updateProviderInfoLog(int $logId, string $providerMessageId, string $providerStatus,float $price);

    public function updateLogBySmsableId(string $smsableId, string $status);
}