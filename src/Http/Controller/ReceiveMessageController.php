<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 7:31 PM
 */

namespace Doctoreto\SMS\Http\Controller;


use App\Http\Controllers\Controller;
use Doctoreto\SMS\Model\ReceivedMessage;
use Doctoreto\SMS\OnReceive;
use Doctoreto\SMS\SMSClient;
use Illuminate\Http\Request;

class ReceiveMessageController extends Controller
{
    /**
     * @var OnReceive
     */
    private $onReceiveHandler;

    /**
     * ReceiveMessageController constructor.
     * @param OnReceive $onReceiveHandler
     */
    public function __construct(OnReceive $onReceiveHandler = null)
    {
        $this->onReceiveHandler = $onReceiveHandler;

        $middleware = config('drtosms.middleware.receive_action_middleware');

        if (!empty($middleware)) {
            $this->middleware($middleware);
        }
    }


    public function receive(Request $request, SMSClient $SMSClient)
    {
        $request = $SMSClient->receive($request);

        ReceivedMessage::create($request->all());

        if ($this->onReceiveHandler) {
            $this->onReceiveHandler->onReceive($request->from,$request->to,$request->message);
        }

    }
}