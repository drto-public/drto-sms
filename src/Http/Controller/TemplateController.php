<?php
/**
 * Created by PhpStorm.
 * User: mpakbaz
 * Date: 1/30/19
 * Time: 8:10 PM
 */

namespace Doctoreto\SMS\Http\Controller;

use App\Http\Controllers\Controller;
use Doctoreto\SMS\Model\Template;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TemplateController extends Controller
{

    public function __construct()
    {
        $middleware = config('drtosms.middleware.template_action_middleware');

        if (!empty($middleware)) {
            $this->middleware($middleware);
        }
    }

    public function getTokens()
    {
        return response()->json(config('drtosms.template_tokens'));
    }

    public function addTemplate(Request $request)
    {
        $this->validate($request, [
            'slug' => 'required|string|min:3|unique:sms_templates',
            'template' => 'required|string|min:5',
            'type' => 'string|min:3'
        ]);

        $template = Template::create($request->all());

        return response()->json($template->toArray());
    }

    public function editTemplate(Request $request, string $slug)
    {
        $this->validate($request, [
            'slug' => [
                'string',
                'min:3',
                Rule::unique('sms_templates')->ignore($slug, 'slug')
            ],
            'template' => 'string|min:5',
            'type' => 'string|min:3',
            'active' => [Rule::in([1, 0])]
        ]);

        $template = Template::where('slug', '=', $slug)->first();

        if (!$template) {
            return response()->json(['message' => 'template does not exist', 404]);
        }

        if ($request->has('slug')) {

            $template->slug = $request->slug;
        }

        if ($request->has('template')) {

            $template->template = $request->template;
        }

        if ($request->has('type')) {
            $template->type = $request->type;
        }

        if ($request->has('active')) {
            $template->active = (bool)$request->active;
        }

        $template->save();

        return response()->json($template->toArray());

    }

    public function deleteTemplate(string $slug)
    {
        $template = Template::where('slug', '=', $slug)->first();

        if (!$template) {
            return response()->json(['message' => 'template does not exist', 404]);
        }

        $template->delete();

        return response()->json(['message' => 'template has been deleted']);
    }

    public function getTemplate(string $slug)
    {
        $template = Template::where('slug', '=', $slug)->first();

        if (!$template) {
            return response()->json(['message' => 'template does not exist', 404]);
        }

        return response()->json($template->toArray());
    }

    public function templateList()
    {
        return response()->json(Template::paginate());
    }

}