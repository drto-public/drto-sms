<?php


Route::get('sms/receive','Doctoreto\SMS\Http\Controller\ReceiveMessageController@receive');

Route::get('sms/template/tokens','Doctoreto\SMS\Http\Controller\TemplateController@getTokens');
Route::get('sms/template/list','Doctoreto\SMS\Http\Controller\TemplateController@templateList');

Route::post('sms/template/add','Doctoreto\SMS\Http\Controller\TemplateController@addTemplate');
Route::post('sms/template/{slug}/edit','Doctoreto\SMS\Http\Controller\TemplateController@editTemplate');
Route::post('sms/template/{slug}/delete','Doctoreto\SMS\Http\Controller\TemplateController@deleteTemplate');