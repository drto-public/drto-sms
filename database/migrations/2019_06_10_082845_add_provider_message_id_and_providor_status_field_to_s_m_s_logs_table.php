<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProviderMessageIdAndProvidorStatusFieldToSMSLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('s_m_s_logs', function (Blueprint $table) {
            $table->string('provider_message_id')->nullable();
            $table->string('provider_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('s_m_s_logs', function (Blueprint $table) {
            //
        });
    }
}
